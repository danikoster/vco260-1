%matplotlib inline
from src.dataprocess import *
from keras.layers import Input, Embedding, concatenate, Reshape, Dense, Dropout, BatchNormalization
from keras.models import Model
import matplotlib.pyplot as plt
from keras.losses import binary_crossentropy
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger
import time
from keras import optimizers
from src.clr_callback import CyclicLR


def uniprot_go_and_pmid_data(organism):
    
    '''
    download data using datadownload or test_datadownload
    '''
    
    
    pass

def filter_out_rare(df,cut_off=5,col_name='go',ind='Entry'):
    ''' 
    filters out go terms that have less then cut_off number of genes associated
    '''
    col1=col_name+'_categorical'
    col2=ind+'_categorical'
    gb=df.groupby(col1).count().sort_values(col2,ascending=False)
    go_to_use=gb[gb[col2]>cut_off].index.values
    return df[df[col1].isin(go_to_use)]
    
def rename_categoric_numeric_columns(df):
    '''
    input: any pandas dataframe
    output: dataframe after object coding,codes
    '''  
    categorical = {c : '%s_categorical'%(c) for c in df.columns if df[c].dtype == 'object' }
    numeric = {c : '%s_numeric'%(c) for c in df.columns if df[c].dtype != 'object'}
    df = df.rename(columns = numeric)
    df = df.rename(columns = categorical)   
    cat_cols = [c for c in df.columns if c.endswith('_categorical')]
    cat_codes = []
    for c in  tqdm(cat_cols):
        df[c], mapping_index = pd.Series(df[c]).factorize()
        cat_codes.append(mapping_index) 
    return df,cat_codes


#---------------------
def get_uniprot_colab_data(col,cut_off=5):
    '''cache and return a combined list of all organisms'''
    merged=load_raw_gene_data()
    merged_df=pd.read_parquet(merged)
    df=merged_df[['Entry',col]]
    df=expand_list(df,col,'Entry',';')
    df,codes=rename_categoric_numeric_columns(df)
    df=filter_out_rare(df,cut_off,col)
    return df,codes
#---------------------
def negative_sampling(df,batch_size=256):
    go_max=df.go_categorical.max()
    temp1=pd.DataFrame()
    while temp1.shape[0]<batch_size:
        temp=pd.DataFrame()
        ent=df.Entry_categorical.sample(1).values[0]
        indexed_not_in=list(set(np.arange(go_max)).difference(df[df.Entry_categorical==ent].go_categorical.values))
        temp['go_categorical']=np.random.choice(indexed_not_in,size=min(batch_size,len(indexed_not_in)))
        temp['target']=0
        temp['Entry_categorical']=ent
        temp1=temp1.append(temp[['Entry_categorical','go_categorical','target']])
    return temp1.sample(batch_size)

def get_original(original_col,num,codes):
    'input integer returns original value'
    if original_col=='Entry':
        return codes[0][num]
    else: return codes[1][num]
        
def test_conversion(processed_df,original_df,codes,id_col='Entry',val_col='go'):
    '''random sample from procces and original and compare with codes'''
    num=processed_df[id_col+'_categorical'].sample(1).values[0]
    ent=get_original(id_col,num,codes)
    go_num=df[df[id_col+'_categorical']==num][val_col+'_categorical'].values[0]
    return get_original(val_col,go_num,codes) in original_df[original_df[id_col]==ent][val_col].values[0]


def collab_model(df,emb_cols,
                 batchnorm,
                 use_skip=False,
                 n_features = 300,
                 dense_size = [50, 50], 
                 dropout = 0.2,
                 optimizer = 'adam',
                 loss='binary_crossentropy',
                 metrics = ['accuracy']
                ):
    '''return a keras model for embedding the data'''    
    layers=[]
    inputs=[]
    for col in emb_cols:
        inp = Input(shape=(1,), name='input_%s'%(col))
        emb = Embedding(df[col].max(), int(n_features), input_length=1, name='embedd_%s'%(col))(inp)
        inputs.append(inp)
        layers.append(emb)
    
    con = concatenate(inputs = [l for l in layers], name='concat_embeddings')
    con = Reshape((int(con.shape[2]),), name='reshape_embedding')(con)
    for idx, width in enumerate(dense_size):
        if idx == 0:
            x = Dense(width, activation='relu', name='dense_%s'%(idx))(con)
        else:
            x = Dense(width, activation='relu')(x)
        if dropout: x = Dropout(dropout)(x)
        if batchnorm: x = BatchNormalization()(x)
    
    #add a "skip connection..."
    if use_skip:
        skip = Dense(architecture[0], activation='relu', name='skipconnection')(con)
        x = concatenate([skip, x], name='concat_skip_deep')    
    x = Dense(1, activation='sigmoid')(x)
    
    

    model = Model(inputs = inputs, outputs = x)
    model.compile(optimizer = optimizer, loss = loss, metrics = metrics)
    return model

def data_genrator(df,val_idx, batch_size=32):
    pos = 0 # ~position
    while True:
        if pos > (len(df)-batch_size): 
            pos = 0
            #shuffle at each epoch!
            df=df.sample(frac=1)
        temp=df[~df.index.isin(val_idx)][pos:pos+int(batch_size/2)]
        temp['target']=1
        temp=pd.concat([temp,neg_gen(df,int(batch_size/2))])
        pos += batch_size
        yield [temp.values[:,0],temp.values[:,1]],temp.values[:,2]
        
        
def get_val_data(df,idx):
    temp=df[df.index.isin(idx)]
    temp['target']=1
    return [temp.values[:,0],temp.values[:,1]],temp.values[:,2]

   

#-----------------------
def train_colab(df,val_frac=0.001,batch_size= 10000,patience =50,min_lr=0.0001,max_lr=0.3,dropout = 0.3):
    '''
    input dataframe composed of 2 integer columns.
    train model with 1-cycle and binary-crossentropy loss 
       returns a trained model and a history of the training.
     '''
    
    model=create_model(df,df.columns.tolist())
    #validation data sample and generator
    idx=df.sample(frac=val_frac).index
    gen=data_genrator(df,idx)
    val_x,val_y=get_val_data(df,idx)    


    #hyperparamters
    epochs = 1 #1cycle
    epoch_size = df.shape[0] #number of training examples per epoch. should be larger
    steps_per_epoch = int(epoch_size / batch_size)
    patience = 50 #stop training if the loss not getting lower..
    step_size = 2 * steps_per_epoch
    

    run_num = 'amit_train_' + time.strftime('%Y_%m_%d_%H_%M')

    #callbacks
    checkpoint = ModelCheckpoint('models/' + run_num + '_cf_take_4_best.h5', save_best_only=True, monitor='loss', save_weights_only=True)
    logger = CSVLogger('models/' + run_num + '_GO_TERM_CLASSIFIER_take_4_log.csv')
    estop = EarlyStopping(monitor='loss', patience = patience)
    clr = CyclicLR(base_lr=min_lr, max_lr=max_lr, step_size=step_size)
    callbacks = [checkpoint, logger, estop, clr]

    #model
    sgd = optimizers.SGD(clipnorm=1.) #from keras.

    hist=model.fit_generator(gen, 
                        steps_per_epoch = steps_per_epoch, 
                        epochs = epochs, 
                        callbacks = callbacks,
                       validation_data=(val_x,val_y))    

    return hist

