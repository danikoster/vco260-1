import pandas as pd
from io import StringIO
import datetime
import requests
import time
import multiprocessing
from tqdm import tqdm as tqdm
import os
import pickle
import numpy as np
from src.parameters import crops,featurelist1,latin_names
parent_dir='vco260'
class Gene():
    def __init__(self,datapath='data',crops=crops):
        #check location
        while not os.getcwd().endswith(parent_dir) or os.getcwd()=='/':
            os.chdir('..')
        assert os.getcwd().endswith(parent_dir) ,"intended to work inside genediscovery1 git folder"
        
        self.datapath=datapath
        self.crops=crops
        self.data_summary=pd.DataFrame(columns=['organism','num_of_recordes','date_of_download'])
        #check existence of previous downloaded data
        if not os.path.exists(datapath):
            os.makedirs(datapath)
        self.summary()
        rest_of=list(set(self.crops.keys()).difference(set(self.data_summary.organism.values.tolist())))
        if rest_of!=[]:
            print('downloading crops: ',rest_of)
            self.download_background(rest_of)
            self.summary()
        print(self.data_summary)
        print('sum of genes: ',self.data_summary.num_of_proteins.sum())
            
    def summary(self):
        files=[x for x in os.listdir(self.datapath) if x.endswith('.gene')]
        self.data_summary=pd.DataFrame(columns=['organism','num_of_proteins','date_of_download'])
        for f in tqdm(files):
            file=os.path.join(self.datapath,f)
            time_of=time.ctime(os.path.getmtime(file))
            num_of=pd.read_csv(file,low_memory=False).shape[0]
            organism=f.replace('.gene','') 
            self.data_summary=self.data_summary.append({'organism':organism,'num_of_proteins':num_of,'date_of_download':time_of},ignore_index=True)
            
    def ncbigetsummary(self,db,query,webenv,st):
        url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db='+db+'&retstart='+str(st)+'&retmax=500&retmode=json&query_key='+query+'&WebEnv='+webenv
        req=requests.get(url)
        if req.status_code==200 and req.text!="":
            return req.json()
        print(req.status_code)

    def ncbigetfetch(self,db,query,webenv,mystring):
        url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db='+db+'&retstart='+str(mystring)+'&retmax=500&retmode=xml&query_key='+query+'&WebEnv='+webenv
        req=requests.get(url)
        if req.status_code==200 and req.text!="":
            return req
        print(req.status_code)   

    def ncbigetcrop(self,db,organism):
        '''
        returns a json to be used in ncbigethist and ncbiloopfetch/summary
        '''
        crop=self.crops.get(organism)
        url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db='+db + '&term=txid' + crop + '[Organism]&usehistory=y&retmax=1000000600&retmode=json'
        req=requests.get(url)
        if req.status_code==200 and req.text!="":
            return req.json()
        print(req.status_code)

    def ncbigethist(self,query_result):
        return query_result['esearchresult']['querykey'] ,query_result['esearchresult']['webenv'] ,query_result['esearchresult']['idlist']


    def ncbiloopfetch(self,db,query_result):
        query,webenv,id_list=self.ncbigethist(query_result)
        num=len(id_list)
        frames=[self.ncbigetfetch(db,query,webenv,x) for x in tqdm(np.arange(int(num/500)+2)*500)]
        return frames

    def get_ncbi_gene_crop(self,crop,db='gene'):
        #print(directory)
        query,webenv,id_list=self.ncbigethist(self.ncbigetcrop(db,crop))
        num=len(id_list)
        frames=[self.ncbigetsummary(db,query,webenv,x) for x in tqdm(np.arange(int(num/500)+2)*500)]
        temp=[]
        for i in range(len(frames)):
            if 'result' in list(frames[i].keys()):
                temp.append(pd.DataFrame(pd.DataFrame(frames[i])[:-3]['result'].values.tolist()))

        fileadd=os.path.join(self.datapath,crop+'.'+db)
        pd.concat(temp).to_csv(fileadd)

    def ncbilooplink(self,db,res,dbto):
        query,webenv,id_list=self.ncbigethist(res)
        num=len(id_list)
        frames=[self.ncbigetlink(db,query,webenv,x,dbto) for x in tqdm(np.arange(int(num/500)+2)*500)]
        return frames

    def ncbigetlink(self,db,query,webenv,mystring,dbto):
        #print(st)
        url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom='+db+'&db='+dbto+'&retstart='+str(mystring)+'&retmax=500&retmode=json&query_key='+query+'&WebEnv='+webenv
        req=requests.get(url)
        if req.status_code==200 and req.text!="":
            return req.json()
        print(req.text) 
        

        
    def download_background(self,list_of_organisms):

        def worker(list_of_organisms):
            #print(crp)
            for org in tqdm(list_of_organisms):
                print('downloading ' + org)
                self.get_ncbi_gene_crop(org)
            #print ('Worker')


        
        p = multiprocessing.Process(target=worker, args=(list_of_organisms,))
        #p = multiprocessing.Process(target=worker)
        p.start()
        #         jobs[-1].terminate()

        

class Uniprot():
    def __init__(self,datapath='data',featurelist1=featurelist1,crops=crops):
        #check location
        while not os.getcwd().endswith(parent_dir) or os.getcwd()=='/':
            os.chdir('..')
        assert os.getcwd().endswith(parent_dir) ,"intended to work inside genediscovery1 git folder"
        self.datapath=datapath
        self.featurelist=featurelist1
        self.crops=crops
        self.data_summary=pd.DataFrame(columns=['organism','num_of_proteins','date_of_download'])
        #check existence of previous downloaded data
        if not os.path.exists(datapath):
            os.makedirs(datapath)
        self.summary()
        rest_of=list(set(self.crops.keys()).difference(set(self.data_summary.organism.values.tolist())))
        if rest_of!=[]:
            print('downloading crops: ',rest_of)
            self.download_fast(rest_of)
            self.summary()
        print(self.data_summary)
        print('sum of proteins: ',self.data_summary.num_of_proteins.sum())
    def summary(self):
        files=[x for x in os.listdir(self.datapath) if x.endswith('.uniprot')]
        self.data_summary=pd.DataFrame(columns=['organism','num_of_proteins','date_of_download'])
        for f in tqdm(files):
            file=os.path.join(self.datapath,f)
            time_of=time.ctime(os.path.getmtime(file))
            num_of=pd.read_csv(file,low_memory=False).shape[0]
            organism=f.replace('.uniprot','')            
            self.data_summary=self.data_summary.append({'organism':organism,'num_of_proteins':num_of,'date_of_download':time_of},ignore_index=True)
        
            
    def download_organism(self,organism):   
        tax_id = self.crops.get(organism)
        cols = 'id,entry,genes,organism,citation,citationmapping,genes,go'+','+self.featurelist
        url  = 'https://www.uniprot.org/uniprot/?query=organism:%s&columns=%s&format=tab'%(tax_id,cols)
        req = requests.get(url)
        file_address=os.path.join(self.datapath,organism+'.uniprot')     
        if req.status_code==200 and req.text!="":
            df=pd.read_table(StringIO(req.text),sep='\t',error_bad_lines=False)
            df.to_csv(file_address)
            
    def check_organism(self,organism):   
        file_address=os.path.join(self.datapath,organism+'.uniprot')
        if not os.path.isfile(file_address):
            return False
        df=pd.read_csv(file_address,low_memory=False)
        ents=set(df['Entry'].values.tolist())
        num_of=df.shape[0]-2
        tax_id = self.crops.get(organism)
        cols = 'id,entry,genes,organism,citation,citationmapping,genes,go'+','+self.featurelist
        url  = 'https://www.uniprot.org/uniprot/?query=organism:%s&&limit=10&offset=%scolumns=%s&format=tab'%(tax_id,str(num_of),cols)
        req = requests.get(url)
        if req.status_code==200 and req.text!="":
            new_=set(pd.read_table(StringIO(req.text),sep='\t',error_bad_lines=False)['Entry'].values.tolist())
            return new_.difference(ents)==set()
        
    def download_fast(self,list_of_organisms):

        def worker(organism):
            #print(crp)
            self.download_organism(organism)
            #print ('Worker')


        jobs = []
        for org in tqdm(list_of_organisms):
            p = multiprocessing.Process(target=worker, args=(org,))
            #p = multiprocessing.Process(target=worker)
            jobs.append(p)
            p.start()
        #         jobs[-1].terminate()

        for j in jobs:
            j.join()
        
    

def save_to_pickle(var,file_address='picklevar.pickle'):
    with open(file_address, 'wb') as handle:
        pickle.dump(var, handle)
        

def load_from_pickle(file_address='picklevar.pickle'):
    with open(file_address, 'rb') as handle:
        return pickle.load(handle)

class Expression():
    def __init__(self,datapath='data',latin_names=latin_names,crops=crops):
        #check location
        while not os.getcwd().endswith(parent_dir) or os.getcwd()=='/':
            os.chdir('..')
        assert os.getcwd().endswith(parent_dir) ,"intended to work inside genediscovery1 git folder"
        self.datapath=datapath
        self.latin_names=latin_names
        self.crops=crops
        #check existence of previous downloaded data
        if not os.path.exists(datapath):
            os.makedirs(datapath)
        self.summary()
        rest_of=list(set(self.crops.keys()).difference(set(self.data_summary.organism.values.tolist())))
        if rest_of!=[]:
            print('downloading crops: ',rest_of)
            self.download_fast(rest_of)
            self.summary()
        print(self.data_summary)
        print('sum of experiments: ',self.data_summary.num_of_experiments.sum())
    def summary(self):
        files=[x for x in os.listdir(self.datapath) if x.endswith('.exp')]
        self.data_summary=pd.DataFrame(columns=['organism','num_of_experiments','date_of_download'])
        for f in tqdm(files):
            file=os.path.join(self.datapath,f)
            time_of=time.ctime(os.path.getmtime(file))
            num_of=len(load_from_pickle(file))
            organism=f.replace('.exp','')            
            self.data_summary=self.data_summary.append({'organism':organism,'num_of_experiments':num_of,'date_of_download':time_of},ignore_index=True)
            
    def array_express(self,crop):
        ogr=self.latin_names.get(crop)
        #print(ogr)
        req=requests.get('https://www.ebi.ac.uk/arrayexpress/json/v3/experiments?species='+ogr+'&gxa=True')
        if req.status_code==200:
            data=pd.DataFrame(req.json())
            if 'experiment' not in data.T.columns.tolist():
                print('no experiment for '+crop)
                return 'no',[]
            array_express1=pd.DataFrame(data.T.experiment.values[0]) 
            entry_list=array_express1.accession.values.tolist()
            return array_express1,entry_list
        print(req.status_code)


    def crop_array_express(self,crop):
        #print(directory)
        array_express1,entry_list=self.array_express(crop)
        p1='https://www.ebi.ac.uk/gxa/experiments-content/'
        #u2='/resources/ExperimentDownloadSupplier.Microarray/query-results'
        u2='/download/RNASEQ_MRNA_BASELINE'
        #p2='/download/RNASEQ_MRNA_DIFFERENTIAL'
        reqs=[requests.get(p1+ x+ u2).text for x in tqdm(entry_list)]
        frames=[]
        for i in tqdm(range(len(reqs))):
            gene_id=reqs[i].find('Gene ID')
            if gene_id!=-1:
                frames.append(pd.read_table(StringIO(reqs[i][gene_id:]),sep='\t',error_bad_lines=False))
            else:
                print('fail')
        file_address=os.path.join(self.datapath, crop+'.exp')
        save_to_pickle(frames,file_address)
        

        
    def download_fast(self,list_of_organisms):

        def worker(organism):
            #print(crp)
            self.crop_array_express(organism)
            #print ('Worker')


        jobs = []
        for org in tqdm(list_of_organisms):
            p = multiprocessing.Process(target=worker, args=(org,))
            #p = multiprocessing.Process(target=worker)
            jobs.append(p)
            p.start()
        #         jobs[-1].terminate()

        for j in jobs:
            j.join()
        
        
        